package com.converter;

import static com.converter.Constants.CMD_ARG_PROJ;
import static com.converter.Constants.CMD_ARG_PUBSUB_TOPIC;
import static com.converter.Constants.CMD_ARG_STORAGE_BUCKET;
import static com.converter.Constants.CMD_ARG_STORAGE_FILE;
import static com.converter.Constants.DEFAULT_BUCKET;
import static com.converter.Constants.DEFAULT_FILE;
import static com.converter.Constants.DEFAULT_PROJ;
import static com.converter.Constants.DEFAULT_TOPIC;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutures;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.TopicName;

import com.opencsv.CSVReader;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads a CSV from Storage Bucket, convert it to JSON and send it to Pub/Sub.
 * The messages are sent in chunks to avoid sending more than 10MB.
 * The JSON format is:
 * {
 *    "values":[
 *       {
 *          "header1":"row1[0]",
 *          "header2":"row1[1]",
 *          ...
 *       },
 *       {
 *          "header1":"row2[0]",
 *          "header2":"row2[1]",
 *          ...
 *       },
 *       ...
 *    ]
 * }
 */
public class Main {

  private static final int LINES_PER_MSG = 50000;

  public static void main(String[] args) throws Exception {
    Params params = new Params(args);

    CSVReader reader = null;
    String[] row = null;
    List<String> messages = new ArrayList<>();

    try {
      Storage storage = StorageOptions.newBuilder().setProjectId(params.proj).build().getService();
      Blob blob = storage.get(params.bucket, params.file);
      String fileContent = new String(blob.getContent());
      reader = new CSVReader(new StringReader(fileContent));

      String[] header = reader.readNext();

      while ((row = reader.readNext()) != null) {
        JSONObject json = new JSONObject();
        JSONArray items = new JSONArray();
        for (int i = 0; i < LINES_PER_MSG; i++) {

          JSONObject item = new JSONObject();
          for (int j = 0; j < header.length; j++) {
            item.put(header[j], row[j]);
          }

          items.put(item);

          if ((row = reader.readNext()) == null) {
            break;
          }
        }
        json.put("values", items);

        messages.add(json.toString());
        System.out.println(json.toString());
      }

      sendMessages(messages, params);

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private static void sendMessages(List<String> msgs, Params params) throws Exception {
    TopicName topicName = TopicName.create(params.proj, params.topic);
    Publisher publisher = null;
    List<ApiFuture<String>> messageIdFutures = new ArrayList<>();

    try {
      // Create a publisher instance with default settings bound to the topic
      publisher = Publisher.defaultBuilder(topicName).build();

      // schedule publishing one message at a time : messages get automatically batched
      for (String message : msgs) {
        ByteString data = ByteString.copyFromUtf8(message);
        PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

        // Once published, returns a server-assigned message id (unique within the topic)
        ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
        messageIdFutures.add(messageIdFuture);
      }
    } finally {
      // wait on any pending publish requests.
      List<String> messageIds = ApiFutures.allAsList(messageIdFutures).get();

      for (String messageId : messageIds) {
        System.out.println("published with message ID: " + messageId);
      }

      if (publisher != null) {
        // When finished with the publisher, shutdown to free up resources.
        publisher.shutdown();
      }
    }
  }

  static class Params {
    String proj;    //defaults: 'bliberal-project'
    String topic;   //defaults: 'bliberal-test'
    String bucket;  //defaults: 'bliberal-bucket'
    String file;    //defaults: 'example.csv'

    Params(String[] args) throws ParseException {
      Options o = new Options();
      o.addOption("p", CMD_ARG_PROJ, true, "Project id Ex.: bliberal-project");
      o.addOption("t", CMD_ARG_PUBSUB_TOPIC, true, "PubSub topic Ex.: bliberal-test");
      o.addOption("b", CMD_ARG_STORAGE_BUCKET, true, "Storage Bucket Ex.: bliberal-bucket");
      o.addOption("i", CMD_ARG_STORAGE_FILE, true, "Storage csv file inside the bucket Ex.: example.csv");
      CommandLine cmdLine = new DefaultParser().parse(o, args, true);

      proj = cmdLine.getOptionValue(CMD_ARG_PROJ, DEFAULT_PROJ);
      topic = cmdLine.getOptionValue(CMD_ARG_PUBSUB_TOPIC, DEFAULT_TOPIC);
      bucket = cmdLine.getOptionValue(CMD_ARG_STORAGE_BUCKET, DEFAULT_BUCKET);
      file = cmdLine.getOptionValue(CMD_ARG_STORAGE_FILE, DEFAULT_FILE);

      for (Option opt : o.getOptions()) {
        System.out.printf("%s: %s\n", opt.getLongOpt(), opt.getValue());
      }
    }
  }
}
