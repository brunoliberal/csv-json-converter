package com.converter;

public class Constants {
  public static final String CMD_ARG_PROJ = "project-id";
  public static final String CMD_ARG_PUBSUB_TOPIC = "pubsub-topic";
  public static final String CMD_ARG_STORAGE_BUCKET = "storage-bucket";
  public static final String CMD_ARG_STORAGE_FILE = "storage-file";

  public static final String DEFAULT_PROJ = "bliberal-project";
  public static final String DEFAULT_TOPIC = "bliberal-test";
  public static final String DEFAULT_BUCKET = "bliberal-bucket";
  public static final String DEFAULT_FILE = "example.csv";
}
